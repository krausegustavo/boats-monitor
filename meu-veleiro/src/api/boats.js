import axios from 'axios';

import { shuffle } from "underscore";


// const _boats = [
//   {
//     "date": "30/06/2017",
//     "description": "ALADIN 30 ANO 2004/2005 - EXCELENTE ESTADO DE CONSERVAÇÃO   Motor yanmar -  Rabeta Guincho elétrico para ancora com duplo comando  Ancora ROCA  Targ",
//     "image": "http://www.eboat.com.br/fotosanuncios/26408-13769-1-P.png",
//     "length": "30",
//     "price": "200000",
//     "title": "ALADIN 30",
//     "url": "http://www.eboat.com.br/anuncio.asp?id=26408"
//   },
//   {
//     "date": "14/06/2017",
//     "description": "Delta 36 - Ano 2006 - com ar condicionado, forno micro ondas, TV/Som, inversor, carregador de baterias, geladeira, fogão inox, sanitário elétrico, targa com pla",
//     "image": "http://www.eboat.com.br/fotosanuncios/26324-2571-1-P.png",
//     "length": "36",
//     "price": "360000",
//     "title": "DELTA 36",
//     "url": "http://www.eboat.com.br/anuncio.asp?id=26324"
//   }
// ]

export default {
  getBoats (cb) {

    let versionDate = '20042018';

    axios.all([
        axios.get('../json/abracore.json?v=' + versionDate),
        axios.get('../json/bombarco.json?v=' + versionDate),
        axios.get('../json/eboat.json?v=' + versionDate),
        axios.get('../json/floripaboat.json?v=' + versionDate),
        axios.get('../json/freeboat.json?v=' + versionDate),
        axios.get('../json/nautica.json?v=' + versionDate),
        axios.get('../json/portalnautico.json?v=' + versionDate),
        axios.get('../json/shopboats.json?v=' + versionDate),
        axios.get('../json/webmarine.json?v=' + versionDate),
        axios.get('../json/olx.json?v=' + versionDate),
        axios.get('../json/mercadolivre.json?v=' + versionDate),
        axios.get('http://meuveleiro.com/api/api/collections/get/sailboats?v=' + versionDate),
      ])
      .then(result => {
          let boats = [];
          for(let site in result) {
            let list;
            // local api content
            if(result[site].data.total > 0) {
              list = result[site].data.entries;
              list = list.filter(item => item.published === true);
            } else {
              // finder content
              list = result[site].data;
            }
            boats.push.apply(boats, list);
          }
          boats = shuffle(boats);
          cb(boats)
        }
      )
      .catch(e => {
        cb(e)
      })

    
  }
}