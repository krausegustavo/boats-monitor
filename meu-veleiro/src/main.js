import Vue from 'vue'
import VueRouter from 'vue-router'
import VueLazyload from 'vue-lazyload'

import store from './store'

Vue.use(VueRouter)
Vue.use(VueLazyload)

import App from './App.vue'
import Boats from './Boats.vue'
import Boat from './Boat.vue'
import Charters from './Charters.vue'
import About from './About.vue'
import Contact from './Contact.vue'
import AddBoat from './AddBoat.vue'

const router = new VueRouter({
  mode: 'history',
  routes: [
    // { path: '/charters', name: 'charters', component: Charters },
    { path: '/sobre', name: 'sobre', component: About },
    { path: '/contato', name: 'contato', component: Contact },
    { path: '/inserir-veleiro', name: 'add-boat', component: AddBoat },
    {
      path: '/veleiro/:id',
      name: 'veleiro',
      component: Boat
    },
    { path: '/', name: 'home', component: Boats,
      children: [
        {
          path: ':pageNumber',
          name: 'boats',
          component: Boats
        },
      ]
    }
  ]
})

import filters from './filters'
filters.create(Vue)

new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App)
})
