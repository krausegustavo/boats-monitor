export default {
    filters: {
        caps: function(value) {
            value = value.toString()
            return value.charAt(0).toUpperCase() + value.slice(1)
        },
        toNumber: function(value) {

            value = String(value);

            value = value.replace(/\s/g, '');

            if(Number(parseFloat(value)) != value) {
                return '';
            }

            let valueArray = value.split('');

            if(valueArray.length < 4) {
                return value;
            }

            let newValue = [];
            let decimal = 1;
            let i = valueArray.length - 1;
            for(; i >= 0; i--) {
                if(decimal > 3) {
                    decimal = 1;
                    newValue.push('.');
                }
                decimal++
                newValue.push(valueArray[i])
            }
            newValue = newValue.reverse().join('');
            return newValue;
        }
    },
    create: function(Vue) {
        Object.keys(this.filters).forEach(function (filter,k) {
            Vue.filter(filter, this.filters[filter])
        }.bind(this))
    }
}