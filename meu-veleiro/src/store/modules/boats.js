import API_BOATS from '../../api/boats'
import * as types from '../mutation-types'

// initial state
const state = {
  all: [],
  loading: true,
  sitesActived: {
    meuveleiro: true,
    abracore: true,
    bombarco: true,
    eboat: true,
    floripaboat: true,
    freeboat: true,
    nautica: true,
    portalnautico: true,
    shopboats: true,
    webmarine: true,
    olx: true,
    mercadolivre: true
  },
  busca: {
    modelo: '',
    minPrice: '',
    maxPrice: '',
    sortByPrice: false
  },
  isShuffled: false,
}

// getters
const getters = {
  allBoats: state => state.all,
  loading: state => state.loading,
  busca: state => state.busca,
  sitesActived: state => state.sitesActived,
  getBoatById: state => (id) => {
    return state.all.find(boat => boat._id == id)
  },
  isShuffled: state => state.isShuffled,
}

// actions
const actions = {
  getAllBoats ({ commit }) {
    API_BOATS.getBoats(boats => {
      commit(types.RECEIVE_BOATS, { boats })
    })
  },
  toggleIsShuffled ({ commit }, toggle) {
    commit(types.TOGGLE_IS_SHUFFLED, toggle)
  },
  toggleSiteActived ({ commit }, site) {
    commit(types.TOGGLE_SITE_ACTIVED, site)
  },
  updateBoatModel ({ commit }, boatModel) {
      commit(types.UPDATE_SEARCH_BOAT_MODEL, boatModel)
  },
  updateMinPrice ({ commit }, price) {
      commit(types.UPDATE_MIN_PRICE, price)
  },
  updateMaxPrice ({ commit }, price) {
      commit(types.UPDATE_MAX_PRICE, price)
  },
  toggleAllSites({ commit }, actived) {
      commit(types.TOGGLE_ALL_SITE, actived)
  },
  toggleSortByPrice({ commit }, sortby) {
      commit(types.TOGGLE_SORTBY_PRICE, sortby)
  }
}

// mutations
const mutations = {
  [types.RECEIVE_BOATS] (state, { boats }) {
    state.loading = false
    state.all = boats
  },
  [types.TOGGLE_SITE_ACTIVED] (state, site) {
    state.sitesActived[site.name] = site.value;
  },
  [types.TOGGLE_IS_SHUFFLED] (state, toggle) {
    state.isShuffled = toggle;
  },
  [types.UPDATE_SEARCH_BOAT_MODEL] (state, boatModel) {
    state.busca.modelo = boatModel;
  },
  [types.UPDATE_MIN_PRICE] (state, price) {
    state.busca.minPrice = price;
  },
  [types.UPDATE_MAX_PRICE] (state, price) {
    state.busca.maxPrice = price;
  },
  [types.TOGGLE_ALL_SITE] (state, actived) {
    for(let site in state.sitesActived) {
      state.sitesActived[site] = actived;
    }
  },
  [types.TOGGLE_SORTBY_PRICE] (state, sortby) {
    state.busca.sortByPrice = sortby;
  }

  // [types.ADD_TO_CART] (state, { id }) {
  //   state.all.find(p => p.id === id).inventory--
  // }
}

export default {
  state,
  getters,
  actions,
  mutations
}