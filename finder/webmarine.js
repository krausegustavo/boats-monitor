
var shared = require('./shared');
var casper = require('casper').create(shared.options);


// paginate which loads each page


var currentPage = 1;
var lastPage;

var boats = [];

var url = 'http://www.webmarine.com.br/classificados-veleiros';

var site = 'webmarine';
var file = shared.jsonURL + site + '.json';




function getBoats() {
    var rows = document.querySelectorAll('.anuncios_veleiros article');
    var content = [];

    for (var i = 0, row; row = rows[i]; i++) {

        var boat = {};

        boat['title'] = row.querySelector('.descricao h4').innerText;
        boat['image'] = row.querySelector('.imagem img').src;
        boat['url'] = row.querySelector('a:not(.botao_favorito)').href;


        var ano = row.querySelector('.descricao .ano').innerText;
        var combustivel = row.querySelector('.descricao .combustivel').innerText;

        boat['description'] = ano + ' - ' + combustivel;

        var price = row.querySelector('.extras h3').innerText;
        price = price.replace(/R\$\s?/g, '');
        price = price.replace(/,[0-9]{2}/g, '');
        price = price.replace(/\./g, '');
        boat['price'] = (price != 0) ? price : '';

        boat['length'] = row.querySelector('.pes span').innerText;

        boat['date'] = row.querySelector('.data_cadastro').innerText;
        boat['site'] = 'webmarine';


        var place = row.querySelector('.extras div').innerText;

        place = place.split(' - ');

        var city = place[0];
        city = city.replace(/(.+?)\n/g, '');

        boat['city'] = city;
        boat['state'] = place[1];

        content.push(boat);
    }

    return content;
}

function processPage() {

    var pageContent = [];

    pageContent = this.evaluate(getBoats);

    boats = boats.concat(pageContent);

    if (currentPage == lastPage) {

        require('fs').write(file, JSON.stringify(boats, null, shared.spaceJSON), 'w');

        return terminate.call(casper);
    } else {
        currentPage = this.evaluate(getSelectedPage);

        var newURL = 'http://www.webmarine.com.br/classificados-veleiros?pag=' + (currentPage + 1);

        this.open(newURL, function() {
            this.echo('Opened page: ' + (newURL));
            this.waitForSelector('.anuncios_veleiros');
        });

        this.then(processPage);
    }

}

function getSelectedPage() {
    return parseInt(document.querySelector('.paginacao strong').innerText);
}

function getLastPage() {
    return parseInt(document.querySelector('.paginacao').getAttribute('data-paginas'));
}

function init() {
    this.echo('Reading: ' + site + '...');
    lastPage = this.evaluate(getLastPage);

    this.then(processPage);
}

function terminate() {
    this.echo('Exiting: ' + site + '...').exit();
}


casper.start(url);
casper.waitForSelector('.anuncios_veleiros', init);
casper.run();

