
var shared = require('./shared');
var casper = require('casper').create(shared.options);


// paginate which loads each page


var currentPage = 1;
var lastPage;

var boats = [];

var url = 'http://www.olx.com.br/veiculos-e-pecas/barcos-lanchas-e-avioes?q=veleiro';

var site = 'olx';
var file = shared.jsonURL + site + '.json';




function getBoats() {
    var rows = document.querySelectorAll('#main-ad-list li[class="item"]');
    var content = [];

    for (var i = 0, row; row = rows[i]; i++) {

        var boat = {};

        boat['title'] = row.querySelector('.OLXad-list-title').innerText;
        var hasImage = !!row.querySelector('.OLXad-list-image-box img');
        if(hasImage) {
            var imgElement = row.querySelector('.OLXad-list-image-box img');
            boat['image'] = imgElement.getAttribute('data-original') || imgElement.src;
        } else {
            boat['image'] = '';
        }

        boat['url'] = row.querySelector('.OLXad-list-link').href;
        // boat['description'] = row.querySelector('.OLXad-list-description').innerText;
        boat['description'] = '';

        var hasPrice = !!row.querySelector('.OLXad-list-price');
        if(hasPrice) {
            var price = row.querySelector('.OLXad-list-price').innerText;
            price = price.replace(/R\$\s?/g, '');
            price = price.replace(/,[0-9]{2}/g, '');
            price = price.replace(/\./g, '');
            boat['price'] = (price != 0) ? price : '';
        } else {
            boat['price'] = '';
        }


        var place = row.querySelector('.detail-region').innerText;

        place = place.split(' - ');

        var city = place[0];

        boat['city'] = city;
        boat['state'] = place[1];


        boat['length'] = '';
        boat['date'] = '';
        boat['site'] = 'olx';

        content.push(boat);
    }

    return content;
}

function nextPage() {
    document.querySelector('.module_pagination li.next a').click();
}

function processPage() {

    var pageContent = [];

    pageContent = this.evaluate(getBoats);

    boats = boats.concat(pageContent);

    if (currentPage == lastPage) {

        require('fs').write(file, JSON.stringify(boats, null, shared.spaceJSON), 'w');

        return terminate.call(casper);
    } else {
        currentPage = this.evaluate(getSelectedPage);

        var newURL = 'http://www.olx.com.br/veiculos-e-pecas/barcos-lanchas-e-avioes?q=veleiro&o=' + (currentPage + 1);

        this.open(newURL, function() {
            this.echo('Opened page: ' + (newURL));
            this.waitForSelector('#main-ad-list');
        });

        this.then(processPage);
    }
}

function getSelectedPage() {
    return parseInt(document.querySelector('.module_pagination li.active').innerText);
}

function getLastPage() {
    var url = document.querySelector('.module_pagination li.last a').href;
    url = url.replace("http://www.olx.com.br/veiculos-e-pecas/barcos-lanchas-e-avioes?o=", '');
    url = url.replace("&q=veleiro", '');

    return parseInt(url);
}

function init() {
    this.echo('Reading: ' + site + '...');
    lastPage = this.evaluate(getLastPage);

    this.then(processPage);
}

function terminate() {
    this.echo('Exiting: ' + site + '...').exit();
}


casper.start(url);
casper.waitForSelector('#main-ad-list', init);
casper.run();

