
var shared = require('./shared');
var casper = require('casper').create(shared.options);


// more loading button


var currentPage = 0;
var lastPage;

var boats = [];

var url = 'http://www.bombarco.com.br/embarcacoes/veleiros-a-venda';

var site = 'bombarco';
var file = shared.jsonURL + site + '.json';



function getBoats() {
    var rows = document.querySelectorAll('.categories-tabela .category-tabela');
    var content = [];

    for (var i = 0, row; row = rows[i]; i++) {

        var boat = {};

        boat['title'] = row.querySelector('.text-tabela-bb-title').innerText;
        boat['image'] = row.querySelector('.bg-img-tabela').src;
        boat['url'] = row.querySelector('a').href;
        var ano = row.querySelector('.text-tabela-bb-ano').innerText
        var estado = row.querySelector('.text-tabela-bb-estado').innerText;
        boat['description'] = ano + ' - ' + estado;

        var price = row.querySelector('.text-tabela-bb-price').innerText;
        price = price.replace(/R\$\s?/g, '');
        price = price.replace(/,[0-9]{2}/g, '');
        price = price.replace(/\./g, '');
        boat['price'] = (price != 0) ? price : '';

        boat['site'] = 'bombarco';
        boat['state'] = '';
        boat['city'] = '';
        boat['length'] = '';
        boat['date'] = '';

        content.push(boat);
    }

    return content;
}

function processPage() {

    var pageContent = [];

    pageContent = this.evaluate(getBoats);

    boats = boats.concat(pageContent);

    require('fs').write(file, JSON.stringify(boats, null, shared.spaceJSON), 'w');

    return terminate.call(casper);
}

function clickMore() {
    var i = 0;
    this.repeat(lastPage,function() {
        this.thenClick('.botao-carregar-list', function() {
            this.echo("Click #" + (++i));
        });
        this.waitForSelector('.preloader[style="display: none;"]');
    });
}

function getLastPage() {
    var moreBtn = document.querySelector('.botao-carregar-list');
    return moreBtn.getAttribute('data-macro') - 1;
}

function init() {
    this.echo('Reading: ' + site + '...');
    lastPage = this.evaluate(getLastPage);
    
    this.then(clickMore);
    this.then(processPage);
}

function terminate() {
    this.echo('Exiting: ' + site + '...').exit();
}


casper.start(url);
casper.waitForSelector('.botao-carregar-list', init);
casper.run();

