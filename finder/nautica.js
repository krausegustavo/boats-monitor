
var shared = require('./shared');
var casper = require('casper').create(shared.options);


// no pagination


var boats = [];

var url = 'http://www.nautica.com.br/classificados/categoria-anuncio/veleiros/';

var site = 'nautica';
var file = shared.jsonURL + site + '.json';


function getBoats() {
    var rows = document.querySelectorAll('.content_left .post-block-out');

    var content = [];

    for (var i = 0, row; row = rows[i]; i++) {

        var boat = {};

        boat['title'] = row.querySelector('.post-right h3').innerText;
        // boat['title'] = title.replace('\n', ' - ');
        boat['image'] = row.querySelector('.post-left img').src;
        boat['url'] = row.querySelector('.post-left a').href;
        boat['description'] = row.querySelector('.post-right .post-desc').innerText;

        var price = row.querySelector('.post-right .post-price').innerText;
        price = price.replace(/R\$\s?/g, '');
        price = price.replace(/,[0-9]{2}/g, '');
        price = price.replace(/\./g, '');
        boat['price'] = (price != 0) ? price : '';


        boat['site'] = 'nautica';
        boat['date'] = row.querySelector('.clock').innerText;

        boat['state'] = '';
        boat['city'] = '';
        boat['length'] = '';

        content.push(boat);
    }

    return content;
}

function processPage() {

    var pageContent = [];

    pageContent = this.evaluate(getBoats);

    boats = boats.concat(pageContent);

    require('fs').write(file, JSON.stringify(boats, null, shared.spaceJSON), 'w');

    return terminate.call(casper);
}

function init() {
    this.echo('Reading: ' + site + '...');
    this.then(processPage);
}

function terminate() {
    this.echo('Exiting: ' + site + '...').exit();
}


casper.start(url);
casper.waitForSelector('.content_left', init);
casper.run();

