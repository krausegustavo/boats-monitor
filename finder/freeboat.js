
var shared = require('./shared');
var casper = require('casper').create(shared.options);


// paginate which loads each page - next link


var currentPage = 0;
var lastPage;

var boats = [];

var url = 'http://www.freeboat.com.br/152-veleiro-sail/listings.html';

var site = 'freeboat';
var file = shared.jsonURL + site + '.json';




function getBoats() {
    var rows = document.querySelectorAll('#ads_col .classified');
    var content = [];

    for (var i = 0, row; row = rows[i]; i++) {

        var boat = {};

        boat['title'] = row.querySelector('.classified_content h3').innerText;
        var hasImage = !!row.querySelector('.classified_photo img');
        if(hasImage) {
            boat['image'] = row.querySelector('.classified_photo img').src;
        } else {
            boat['image'] = '';
        }
        boat['url'] = row.querySelector('.classified_content h3 a').href;

        if(row.querySelectorAll('.price').length === 1) {
            var price = row.querySelector('.price').innerText;
            price = price.replace(/R\$\s?/g, '');
            price = price.replace(/,[0-9]{2}/g, '');
            price = price.replace(/\./g, '');
            boat['price'] = (price != 0) ? price : '';

        } else {
            boat['price'] = '';
        }

        boat['description'] = row.querySelector('.classified_content p').innerText;
        boat['site'] = 'freeboat';

        var place = row.querySelector('.location').innerText;
        place = place.replace(/\|/gi, '');
        place = place.split(', ');

        var state = place[0];
        state = state.replace(/\s/g, '');
        boat['state'] = state;

        boat['city'] = place[1];

        content.push(boat);
    }

    return content;
}

function nextPage() {
    document.querySelector('.paginator ul li:last-child .prevnext').click();
}

function processPage() {

    var pageContent = [];

    pageContent = this.evaluate(getBoats);

    boats = boats.concat(pageContent);

    if (currentPage == lastPage) {

        require('fs').write(file, JSON.stringify(boats, null, shared.spaceJSON), 'w');

        return terminate.call(casper);
    }

    this.evaluate(nextPage);

    currentPage = this.evaluate(getSelectedPage);

    this.waitForSelector('#ads_col');

    this.then(processPage);
}

function getSelectedPage() {
    return parseInt(document.querySelector('.paginator ul li .currentpage').innerText);
}

function getLastPage() {
    var pages = document.querySelector('.paginator ul li:nth-child(3)').innerText;
    pages = pages.replace('Página 1 de ', '');
    return parseInt(pages);
}

function init() {
    this.echo('Reading: ' + site + '...');
    lastPage = this.evaluate(getLastPage);

    this.then(processPage);
}

function terminate() {
    this.echo('Exiting: ' + site + '...').exit();
}


casper.start(url);
casper.waitForSelector('#ads_col', init);
casper.run();

