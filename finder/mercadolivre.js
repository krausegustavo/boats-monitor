
var shared = require('./shared');

shared.options.pageSettings.loadImages = true;

var casper = require('casper').create(shared.options);


// paginate which loads each page


var currentPage = 1;
var lastPage;

var boats = [];

var url = 'https://veiculos.mercadolivre.com.br/veleiro-monocasco-e-multicasco/';

var site = 'mercadolivre';
var file = shared.jsonURL + site + '.json';




function getBoats() {
    var rows = document.querySelectorAll('#searchResults .results-item');
    var content = [];

    for (var i = 0, row; row = rows[i]; i++) {

        var boat = {};

        boat['title'] = row.querySelector('.main-title').innerText;
        var hasImage = !!row.querySelector('.item-link img');
        if(hasImage) {
            var imgElement = row.querySelector('.item-link img');
            boat['image'] = imgElement.src;
        } else {
            boat['image'] = '';
        }

        boat['url'] = row.querySelector('.item-link').href;

        var hasDescription = !!row.querySelector('.item__attrs');
        if(hasDescription) {
            boat['description'] = row.querySelector('.item__attrs').innerText;
        } else {
            boat['description'] = '';
        }

        var hasPrice = !!row.querySelector('.price__fraction');
        if(hasPrice) {
            var price = row.querySelector('.price__fraction').innerText;
            price = price.replace(/\./g, '');
            boat['price'] = (price != 0) ? price : '';
        } else {
            boat['price'] = '';
        }


        var place = row.querySelector('.item__location').innerText;

        boat['city'] = '';
        boat['state'] = place;

        boat['length'] = '';
        boat['date'] = '';
        boat['site'] = 'mercadolivre';

        content.push(boat);
    }

    return content;
}

function nextPage() {
    document.querySelector('.pagination li.pagination__next a').click();
}

function processPage() {

    var pageContent = [];

    pageContent = this.evaluate(getBoats);

    boats = boats.concat(pageContent);

    if (currentPage == lastPage) {

        require('fs').write(file, JSON.stringify(boats, null, shared.spaceJSON), 'w');

        return terminate.call(casper);
    } else {

        this.evaluate(nextPage);
        this.waitForSelector('#searchResults');

        currentPage = this.evaluate(getSelectedPage);

        this.then(processPage);
    }
}

function getSelectedPage() {
    return parseInt(document.querySelector('.pagination .pagination__page--current a').innerText);
}

function getLastPage() {
    var quantity = document.querySelector('.quantity-results').innerText;
    quantity = quantity.replace(" resultados", '');
    var itemsPerPage = 48;
    var lastPage = Math.ceil(quantity / itemsPerPage);
    return parseInt(lastPage);
}

function init() {
    this.echo('Reading: ' + site + '...');
    lastPage = this.evaluate(getLastPage);

    this.then(processPage);
}

function terminate() {
    this.echo('Exiting: ' + site + '...').exit();
}


casper.start(url);
casper.waitForSelector('#searchResults', init);
casper.run();

