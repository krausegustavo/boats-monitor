
var shared = require('./shared');
var casper = require('casper').create(shared.options);


// paginate already loaded


var currentPage = 0;
var lastPage;

var boats = [];

var url = 'http://www.eboat.com.br/classificados.asp?cat=veleiros';

var site = 'eboat';
var file = shared.jsonURL + site + '.json';



function getBoats() {
    var rows = document.querySelectorAll('#demo .list .list-item');
    var content = [];

    for (var i = 0, row; row = rows[i]; i++) {

        var boat = {};

        boat['title'] = row.querySelector('.title').innerText;
        boat['image'] = row.querySelector('img').src;
        boat['url'] = row.querySelector('a').href;
        boat['description'] = row.querySelector('.desc').innerText;

        var price = row.querySelector('.price').innerText;
        price = price.replace(/\./g, '');
        boat['price'] = (price != 0) ? price : '';

        boat['length'] = row.querySelector('.view').innerText;

        var elDate = row.querySelector('.date');
        boat['date'] = elDate.innerText;
        boat['site'] = 'eboat';

        var place = elDate.parentNode.innerText;
        place = place.replace(/(\\n|\\t|\s{2,})/gi, '')
        var regex = /\.\.\.(.+) - (AC|AL|AP|AM|BA|CE|DF|ES|GO|MA|MT|MS|MG|PA|PB|PR|PE|PI|RJ|RN|RS|RO|RR|SC|SP|SE|TO)/gi;

        if(regex.test(place)) {
            var matches = place.match(regex);
            var newPlace = matches[0];
            newPlace = newPlace.replace(/\.\.\./gi, '')
            newPlace = newPlace.split(' - ');
            boat['city'] = newPlace[0];
            boat['state'] = newPlace[1];
        } else {
            boat['city'] = '';
            boat['state'] = '';
        }
        
        content.push(boat);
    }

    return content;
}

function nextPage() {
    document.querySelector('.jplist-next').click();
}

function processPage() {

    var pageContent = [];

    pageContent = this.evaluate(getBoats);

    boats = boats.concat(pageContent);

    if (currentPage == lastPage) {

        require('fs').write(file, JSON.stringify(boats, null, shared.spaceJSON), 'w');

        return terminate.call(casper);
    }

    this.evaluate(nextPage);

    currentPage = this.evaluate(getSelectedPage);

    this.then(processPage);
}

function getSelectedPage() {
    return document.querySelector('.jplist-current').getAttribute('data-number');
}

function reset() {
    document.querySelector('.jplist-first').click();
}

function getLastPage() {
    return document.querySelector('.jplist-last').getAttribute('data-number');
}

function init() {
    this.echo('Reading: ' + site + '...');
    lastPage = this.evaluate(getLastPage);

    this.evaluate(reset);

    this.then(processPage);
}

function terminate() {
    this.echo('Exiting: ' + site + '...').exit();
}


casper.start(url);
casper.waitForSelector('#demo .list', init);
casper.run();

