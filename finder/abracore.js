
var shared = require('./shared');
var casper = require('casper').create(shared.options);


// no pagination


var currentPage = 0;
var lastPage;

var boats = [];

var url = 'http://www.abracore.com.br/barcos?tipo=Vela';

var site = 'abracore';

var file = shared.jsonURL + site + '.json';


function getBoats() {
    var rows = document.querySelectorAll('.container > .row:nth-child(3) > .col-md-3');

    var content = [];

    for (var i = 0, row; row = rows[i]; i++) {

        var boat = {};

        var title = row.querySelector('.modelo-nome').innerText;
        boat['title'] = title.replace('\n', ' - ');
        boat['image'] = row.querySelector('img').src;
        boat['url'] = row.querySelector('a').href;
        boat['description'] = row.querySelector('.list1_gallery li:nth-child(2)').innerText;

        var price = row.querySelector('.barco-preco').innerText;
        price = price.replace(/R\$\s?/g, '');
        price = price.replace(/,[0-9]{2}/g, '');
        price = price.replace(/\./g, '');
        boat['price'] = (price != 0) ? price : '';

        boat['site'] = 'abracore';
        boat['state'] = '';
        boat['city'] = '';
        boat['length'] = '';
        boat['date'] = '';

        content.push(boat);
    }

    return content;
}

function processPage() {
    var pageContent = [];

    pageContent = this.evaluate(getBoats);

    boats = boats.concat(pageContent);

    require('fs').write(file, JSON.stringify(boats, null, shared.spaceJSON), 'w');

    return terminate.call(casper);
}


function init() {
    this.echo('Reading: ' + site + '...');
    this.then(processPage);
}

function terminate() {
    this.echo('Exiting: ' + site + '...').exit();
}


casper.start(url);
casper.waitForSelector('.container > .row:nth-child(3)', init);
casper.run();

