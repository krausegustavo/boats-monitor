
var shared = require('./shared');
var casper = require('casper').create(shared.options);


// paginate which loads each page - next link


var currentPage = 0;
var lastPage;

var boats = [];

var url = 'https://www.shopboats.com.br/busca.htm?tipo=2';

var site = 'shopboats';
var file = shared.jsonURL + site + '.json';




function getBoats() {
    var rows = document.querySelectorAll('.listagem .busca-row');
    var content = [];

    for (var i = 0, row; row = rows[i]; i++) {

        var boat = {};

        boat['title'] = row.querySelector('.title').innerText;
        boat['image'] = row.querySelector('.img img').src;
        boat['url'] = row.querySelector('a').href;

        var condicao = row.querySelector('.condicao').innerText;
        var ano = row.querySelector('.ano').innerText;
        var comb = row.querySelector('.comb').innerText;
        boat['description'] = condicao + '; ' + ano + '; ' + comb;

        var price = row.querySelector('.preco').innerText;
        price = price.replace(/R\$\s?/g, '');
        price = price.replace(/,[0-9]{2}/g, '');
        price = price.replace(/\./g, '');
        boat['price'] = (price != 0) ? price : '';


        boat['length'] = row.querySelector('.tamanho').innerText;

        boat['site'] = 'shopboats';
        boat['date'] = '';

        var place = row.querySelector('.end').innerText;

        place = place.split(' - ');

        var city = place[0];
        city = city.replace(/(.+?)\n/g, '');

        boat['city'] = city;

        boat['state'] = place[1];

        content.push(boat);
    }

    return content;
}

function nextPage() {
    document.querySelector('.pag-rodape .paginacao a:last-child').click();
}

function processPage() {

    var pageContent = [];

    pageContent = this.evaluate(getBoats);

    boats = boats.concat(pageContent);

    if (currentPage == lastPage) {

        require('fs').write(file, JSON.stringify(boats, null, shared.spaceJSON), 'w');

        return terminate.call(casper);
    }

    this.evaluate(nextPage);

    currentPage = this.evaluate(getSelectedPage);

    this.then(processPage);
}

function getSelectedPage() {
    return parseInt(document.querySelector('.pag-rodape .paginacao .pag-atual').innerText);
}

function getLastPage() {
    return parseInt(document.querySelector('.pag-rodape .paginacao a:nth-last-child(2)').innerText);
}

function init() {
    this.echo('Reading: ' + site + '...');
    lastPage = this.evaluate(getLastPage);

    this.then(processPage);
}

function terminate() {
    this.echo('Exiting: ' + site + '...').exit();
}


casper.start(url);
casper.waitForSelector('.listagem', init);
casper.run();

