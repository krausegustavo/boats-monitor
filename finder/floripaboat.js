
var shared = require('./shared');
var casper = require('casper').create(shared.options);


// no pagination


var boats = [];

var url = 'http://www.floripaboat.com.br/site/?bln_busca_anuncio=sim&str_classificacao=Venda&num_id_categoria=493&num_id_subcategoria=1876';

var site = 'floripaboat';
var file = shared.jsonURL + site + '.json';


function getBoats() {
    var rows = document.querySelectorAll('#anuncios .anuncio');

    var content = [];

    for (var i = 0, row; row = rows[i]; i++) {

        var boat = {};

        boat['title'] = row.querySelector('h1 a').innerText;
        // boat['title'] = title.replace('\n', ' - ');
        boat['image'] = row.querySelector('.anuncio_imagens img').src;
        boat['url'] = row.querySelector('h1 a').href;
        boat['description'] = row.querySelector('.anuncio_imagens img').alt;

        var price = row.querySelector('.valor').innerText;
        price = price.replace(/R\$\s?/g, '');
        price = price.replace(/,[0-9]{2}/g, '');
        price = price.replace(/\./g, '');
        boat['price'] = (price != 0) ? price : '';

        
        boat['date'] = row.querySelector('.dte').innerText;
        boat['site'] = 'floripaboat';

        var place = row.querySelector('.local').innerText;

        place = place.split(' - ');
        boat['city'] = place[0];
        boat['state'] = place[1];

        content.push(boat);
    }

    return content;
}

function processPage() {

    var pageContent = [];

    pageContent = this.evaluate(getBoats);

    boats = boats.concat(pageContent);

    require('fs').write(file, JSON.stringify(boats, null, shared.spaceJSON), 'w');

    return terminate.call(casper);
}

function init() {
    this.echo('Reading: ' + site + '...');
    this.then(processPage);
}

function terminate() {
    this.echo('Exiting: ' + site + '...').exit();
}


casper.start(url);
casper.waitForSelector('#anuncios', init);
casper.run();

