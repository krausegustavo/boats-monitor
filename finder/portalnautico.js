
var shared = require('./shared');
var casper = require('casper').create(shared.options);


// paginate which loads each page


var currentPage = 1;
var lastPage;

var boats = [];

var url = 'https://www.portalnautico.com.br/barcos/2.-veleiros-a-venda';

var site = 'portalnautico';
var file = shared.jsonURL + site + '.json';




function getBoats() {
    var rows = document.querySelectorAll('.recommended-ads .ad-item');
    var content = [];

    for (var i = 0, row; row = rows[i]; i++) {

        var boat = {};

        boat['title'] = row.querySelector('.item-title').innerText;
        boat['image'] = row.querySelector('.item-image img').src;
        boat['url'] = row.querySelector('.item-image a').href;
        boat['description'] = row.querySelector('.item-cat').innerText;

        var price = row.querySelector('.item-price').innerText;
        price = price.replace(/R\$\s?/g, '');
        price = price.replace(/,[0-9]{2}/g, '');
        price = price.replace(/\./g, '');
        boat['price'] = (price != 0) ? price : '';


        // boat['length'] = row.querySelector('.view').innerText;

        boat['date'] = row.querySelector('.ad-meta .dated').innerText;

        boat['city'] = row.querySelector('.user-option a').getAttribute('data-original-title');
        boat['state'] = '';
        boat['site'] = 'portalnautico';

        content.push(boat);
    }

    return content;
}

// function nextPage() {
//     document.querySelector('.pagination li:last-child a').click();
// }

function processPage() {

    var pageContent = [];

    pageContent = this.evaluate(getBoats);

    boats = boats.concat(pageContent);

    if (currentPage == lastPage) {

        require('fs').write(file, JSON.stringify(boats, null, shared.spaceJSON), 'w');

        return terminate.call(casper);
    } else {
        currentPage = this.evaluate(getSelectedPage);

        var newURL = 'https://www.portalnautico.com.br/barcos/2.-veleiros-a-venda/pag/' + (currentPage + 1);

        this.open(newURL, function() {
            this.echo('Opened page: ' + (newURL));
            this.waitForSelector('.recommended-ads');
        });

        this.then(processPage);
    }
}

function getSelectedPage() {
    return parseInt(document.querySelector('.pagination .active').innerText);
}

function getLastPage() {
    return parseInt(document.querySelector('.pagination li:nth-last-child(2)').innerText);
}

function init() {
    this.echo('Reading: ' + site + '...');
    lastPage = this.evaluate(getLastPage);

    this.then(processPage);
}

function terminate() {
    this.echo('Exiting: ' + site + '...').exit();
}


casper.start(url);
casper.waitForSelector('.recommended-ads', init);
casper.run();

