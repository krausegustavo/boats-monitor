exports.jsonURL = '../meu-veleiro/json/';
exports.spaceJSON = -1; // -1 for production
// exports.spaceJSON = '  '; // '  ' for test

exports.options = {
    logLevel: "info",
    verbose: true,
    pageSettings: {
        loadImages: false,
        userAgent: "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.116 Safari/537.36"
    }
};